import { createStore } from 'redux';

import rootReducer from './rootReducer';

/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
const reduxDevtool = ( window as any ).__REDUX_DEVTOOLS_EXTENSION__ && ( window as any ).__REDUX_DEVTOOLS_EXTENSION__();

const store = createStore(
  rootReducer,
  reduxDevtool,
);

export default store;
