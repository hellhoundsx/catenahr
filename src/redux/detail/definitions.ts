import { EDetailPopupModes, IAction } from '~definitions';

export interface IPopupModesAction extends IAction {
  payload: EDetailPopupModes;
}

export interface IDetailState {
  mode: EDetailPopupModes;
  isVisible: boolean;
  activeId: number;
}

