import { EDetailPopupModes, IAction, IBooleanAction, INumberAction } from '~definitions';
import { IPopupModesAction } from '~redux/detail/definitions';

import ownTypes from './types';

/**
 * Action fired when you want to set an active Employee
 * @param id - Id of employee to set to show
 */
export const setDetailActiveId = ( id: number ): INumberAction => ( {
  type:    ownTypes.SET_ACTIVE_ID,
  payload: id,
} );

/**
 * Action fired to toggle the visibility of the detail popup
 * @param isVisible - Visibility of the popup
 */
export const setDetailVisibility = ( isVisible: boolean ): IBooleanAction => ( {
  type:    ownTypes.SET_VISIBILITY,
  payload: isVisible,
} );

/**
 * Action fired to set the mode of the detail popup
 * @param mode - mode to set the popup
 */
export const setDetailPopupMode = ( mode: EDetailPopupModes ): IPopupModesAction => ( {
  type:    ownTypes.SET_MODE,
  payload: mode,
} );


/**
 * Action fired to reset the state of Detail
 */
export const resetDetail = (): IAction => ( {
  type:    ownTypes.RESET,
} );
