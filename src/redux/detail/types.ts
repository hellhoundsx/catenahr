export default {
  SET_ACTIVE_ID:  'detail/SET_ACTIVE_ID',
  SET_VISIBILITY: 'detail/SET_VISIBILITY',
  SET_MODE:       'detail/SET_MODE',
  RESET:          'detail/RESET',
};
