import { EDetailPopupModes, IAppState } from '~/definitions';
import { IEmployee } from '~redux/employees/definitions';
import { getEmployeesListSel } from '~redux/employees/selectors';

export const getDetailActiveIdSel = ( state: IAppState ): number => state.detail.activeId;
export const getDetailVisibilitySel  = ( state: IAppState ): boolean =>  state.detail.isVisible;
export const getDetailModeSel  = ( state: IAppState ): EDetailPopupModes =>  state.detail.mode;

// Use-case of reselect
export const getActiveEmployee = ( state: IAppState ): IEmployee | undefined => {
  const list = getEmployeesListSel( state );

  return list.filter( ( ( { id } ) => id === getDetailActiveIdSel( state ) ) )[0];
}
