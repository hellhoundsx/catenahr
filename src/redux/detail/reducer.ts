import { EDetailPopupModes, IBooleanAction, INumberAction } from '~definitions';
import { reducerUtil } from '~utils/reducer.util';

import { IDetailState, IPopupModesAction } from './definitions';
import ownTypes from './types';

const INITIAL_STATE: IDetailState = {
  mode:      EDetailPopupModes.new,
  isVisible: false,
  activeId:  0,
};

/**
 * Saves the active Id of the user
 */
const setActiveEmployee = ( state: IDetailState, { payload }: INumberAction ): IDetailState => ( {
  ...state,
  activeId:  payload,
  isVisible: true,
} );

/**
 * Sets the `isVisible` state
 */
const setVisibility = ( state: IDetailState, { payload }: IBooleanAction ): IDetailState => ( {
  ...state,
  isVisible: payload,
} );

/**
 * Sets the `mode` state
 */
const setMode = ( state: IDetailState, { payload }: IPopupModesAction ): IDetailState => ( {
  ...state,
  mode: payload,
} );

/**
 * Resets to its initial state
 */
const resetState = (): IDetailState => ( {
  ...INITIAL_STATE
} );

export default reducerUtil( INITIAL_STATE, {
  [ownTypes.SET_ACTIVE_ID]:  setActiveEmployee,
  [ownTypes.SET_VISIBILITY]: setVisibility,
  [ownTypes.SET_MODE]:       setMode,
  [ownTypes.RESET]:          resetState,
} );
