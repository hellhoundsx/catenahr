import { combineReducers } from 'redux';

import employeesReducer from '~redux/employees/reducer';
import detailReducer from '~redux/detail/reducer';

const rootReducer = combineReducers( {
  employees: employeesReducer,
  detail: detailReducer,
} );

export default rootReducer;
