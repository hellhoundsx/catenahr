import { reducerUtil } from '~utils/reducer.util';

import { IEmployeeAction, IEmployeeListAction, IEmployeesState } from './definitions';
import ownTypes from './types';

const INITIAL_STATE: IEmployeesState = {
  list: [],
};

/**
 * Saves the list of employees
 */
const setList = ( state: IEmployeesState, { payload }: IEmployeeListAction ): IEmployeesState => ( {
  ...state,
  list: payload,
} );

/**
 * Adds one employee to the last of the array of `list`
 */
const addEmployee = ( state: IEmployeesState, { payload }: IEmployeeAction ): IEmployeesState => ( {
  ...state,
  list: [...state.list, payload],
} );

export default reducerUtil( INITIAL_STATE, {
  [ownTypes.SET_EMPLOYEE_LIST]: setList,
  [ownTypes.ADD_EMPLOYEE]: addEmployee
} );
