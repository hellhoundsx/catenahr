import { IAction } from '~definitions';

export interface IEmployee {
  id: number;
  first_name: string;
  last_name: string;
  annual_salary: {
    amount:  number,
    currency: string
  },
  start_date: string;
}

export interface IFormattedEmployee {
  id: number;
  name: string;
  salary: number,
  currency: string;
  start_date: Date;
}


export interface IEmployeesState {
  list: IEmployee[];
}

export interface IEmployeeListAction extends IAction {
  payload: IEmployee[];
}
export interface IEmployeeAction extends IAction {
  payload: IEmployee;
}
