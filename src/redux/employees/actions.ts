import { IEmployee, IEmployeeAction, IEmployeeListAction } from '~redux/employees/definitions';

import ownTypes from './types';

/**
 * Action fired when you want to set the List of Employees
 * @param list - List of employees that will be updated
 */
export const setEmployeeList = ( list: IEmployee[] ): IEmployeeListAction => ( {
  type:    ownTypes.SET_EMPLOYEE_LIST,
  payload: list,
} );

/**
 * Action fired when to add a employee to the list of Employees
 * @param employee - Info of the employee
 */
export const addEmployee = ( employee: IEmployee ): IEmployeeAction => ( {
  type:    ownTypes.ADD_EMPLOYEE,
  payload: employee,
} );
