export default {
  SET_EMPLOYEE_LIST: 'employees/SET_EMPLOYEE_LIST',
  ADD_EMPLOYEE:      'employees/ADD_EMPLOYEE',
};
