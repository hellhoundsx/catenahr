import { IAppState } from '~/definitions';
import { IEmployee, IFormattedEmployee } from '~redux/employees/definitions';

export const getEmployeesListSel = ( state: IAppState ): IEmployee[] => state.employees.list;

export const getEmployeesFormattedList = ( state: IAppState ): IFormattedEmployee[] => {
  if ( state.employees.list?.length > 0 ) {
    return state.employees.list.map( ( employee ) => ( {
      id: employee.id,
      name: `${employee.first_name} ${employee.last_name}`,
      salary: employee.annual_salary.amount,
      currency: employee.annual_salary.currency,
      start_date: new Date ( employee.start_date ),
    } ) );
  }

  return [];
}
