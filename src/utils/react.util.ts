import * as React from 'react';

/**
 * Helper function to provide move readability to "Component Did Mount" Hook
 * @param callback - Function with logic to execute on mount
 */
export const componentDidMount = ( callback: CallableFunction ): void => {
  React.useEffect( () => callback(), [] );
};

/**
 * Helper function to provide move readability to "Component Did Update" Hook
 * @param changeVariables - variables to listen to changes
 * @param callback - Function with logic to execute on change
 */
export const onPropsChanged = ( changeVariables: Array<unknown>, callback: CallableFunction ): void => {
  React.useEffect( () => callback(), changeVariables );
};

/**
 * Helper function to provide a cleaner way of controlling inputs
 * @param initialValue - initial value of the input
 */
export const useInput = ( initialValue: any ) => {
  const [ value, setValue ] = React.useState( initialValue );

  return {
    value,
    bind:  {
      value,
      onChange:  ( event: any ) => {
        setValue( event.target.value );
      },
    },
  };
};
