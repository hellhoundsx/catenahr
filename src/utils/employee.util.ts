import { IEmployee } from '~redux/employees/definitions';

/**
 * Formats the data to the desired format
 */
export const formatEmployee = (
  id: number, firstName: string, lastName: string, salary: number, currency: string, startDate: string,
): IEmployee => ( {
  id,
  first_name:    firstName,
  last_name:     lastName,
  annual_salary: {
    amount: salary,
    currency,
  },
  start_date:    startDate,
} );
