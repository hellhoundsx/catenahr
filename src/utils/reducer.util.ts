import { IAction, IHandler, TAllStates } from '~definitions';

/**
 * Structures the reducer to a simpler way instead of switch case
 * @param initialState: initial state of the reducer
 * @param handlers: Object containing the function to execute on determined action type
 */
export const reducerUtil = ( initialState: TAllStates, handlers: IHandler ) =>
  function reducer ( state = initialState, action: IAction ): unknown {
    if ( handlers.hasOwnProperty( action.type ) ) {
      return handlers[action.type]( state, action );
    } else {
      return state;
    }
  };
