import { IDetailState } from '~redux/detail/definitions';
import { IEmployeesState } from '~redux/employees/definitions';

// Reducer.util
export interface IHandler {
  [key: string]: ( state: unknown, action: IAction ) => unknown;
}

// Redux
export type TAllStates = IEmployeesState | IDetailState;

export interface IAppState {
  employees: IEmployeesState,
  detail: IDetailState,
}

export interface IAction {
  type: string;
  payload?: any;
}

export interface IBooleanAction extends IAction {
  payload: boolean;
}

export interface IStringAction extends IAction {
  payload: string;
}

export interface INumberAction extends IAction {
  payload: number;
}

export enum EDetailPopupModes {
  new = 'new',
  view = 'view'
}
