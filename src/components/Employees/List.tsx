import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { grey } from '@material-ui/core/colors';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import VisibilityIcon from '@material-ui/icons/Visibility';

import { EDetailPopupModes, IAppState } from '~definitions';
import { setDetailActiveId, setDetailPopupMode } from '~redux/detail/actions';
import { getEmployeesFormattedList } from '~redux/employees/selectors';
import { IFormattedEmployee } from '~redux/employees/definitions';

import { convertDate } from '~utils/date.util';

interface IOwnProps {
  nameFilter: string;
}

interface IOwnReduxStateProps {
  list: IFormattedEmployee[];
}

interface IOwnReduxDispatchProps {
  handleViewDetail: ( id: number ) => void;
}

type TAllProps = IOwnProps & IOwnReduxStateProps & IOwnReduxDispatchProps;

enum ESortType {
  none,
  ascending,
  descending,
}

const useStyles = makeStyles( {
  table:        {
    minWidth: 650,
  },
  sort:         {
    float: 'right',
  },
  hidden:       {
    opacity: '0',
  },
  headerActive: {
    background: grey[300],
  },
} );

const columns = [
  { field: 'id', name: 'ID' },
  { field: 'name', name: 'Name' },
  { field: 'salary', name: 'Salary' },
  { field: 'start_date', name: 'Start Date' },
];

/**
 * Component that handles the table and all of it main features
 * @param nameFilter - Filter for the name tab
 * @param list - List of employees
 * @param handleViewDetail - Action to show the detail popup
 */
const EmployeeList = ( { nameFilter, list, handleViewDetail }: TAllProps ) => {
  const [ currentOrder, setCurrentOrder ] = React.useState( { index: -1, type: ESortType.none } );

  const classes = useStyles();

  /**
   * Function to set the current field which is being ordered
   * @param nextIndex - Index( of the element in the array ) of the column to be ordered
   */
  const handleChangeOrder = ( nextIndex: number ) => {
    let nextType = ESortType.ascending;

    if ( nextIndex === currentOrder.index ) {
      nextType = currentOrder.type === ESortType.descending ? ESortType.none : currentOrder.type + 1;
    }

    if ( nextType === ESortType.none ) {
      // Reset to the default value
      setCurrentOrder( { index: -1, type: ESortType.none } );
    } else {
      setCurrentOrder( { index: nextIndex, type: nextType } );
    }

  };

  /**
   * Function to sort the array of employees by the current order
   */
  const sortList = (): IFormattedEmployee[] => {
    const config = columns[currentOrder.index];
    const filteredList = list.filter( ( ( { name } ) => name.includes( nameFilter ) ) );
    const newList = Object.assign( [], filteredList );

    switch ( currentOrder.type ) {
      case ESortType.ascending:
        return newList.sort( ( a, b ) => ( a[config.field] > b[config.field] ) ? 1 : -1 );
      case ESortType.descending:
        return newList.sort( ( a, b ) => ( a[config.field] > b[config.field] ) ? -1 : 1 );
      default:
        return newList;
    }
  };

  /**
   * Gets the correct icon with correct classes to show on each column
   */
  const getSortIcon = ( isActive: boolean ) => {
    if ( isActive ) {
      if ( currentOrder.type === 1 ) {
        return <ArrowDownwardIcon className={ classes.sort } />;
      } else if ( currentOrder.type === 2 ) {
        return <ArrowUpwardIcon className={ classes.sort } />;
      }
    }

    return <ArrowUpwardIcon className={ `${ classes.sort } ${ classes.hidden }` } />;
  };

  const correctList = sortList();

  return (
    <TableContainer component={ Paper }>
      <Table className={ classes.table } aria-label='simple table'>
        <TableHead>
          <TableRow>
            { columns.map( ( { field, name }, index ) => {
              const isActive = currentOrder.index === index;
              const activeClass = isActive && currentOrder.type > 0 ? classes.headerActive : '';

              return (
                <TableCell key={ `el-${ field }` } className={ activeClass }
                           onClick={ () => handleChangeOrder( index ) }>
                  { name }
                  { getSortIcon( isActive ) }
                </TableCell>
              );
            } ) }
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          { correctList.length > 0
            ? correctList.map( ( employee ) => (
              <TableRow key={ employee.id }>
                <TableCell>{ employee.id }</TableCell>
                <TableCell>{ employee.name }</TableCell>
                <TableCell>{ employee.salary }</TableCell>
                <TableCell>{ convertDate( employee.start_date ) }</TableCell>
                <TableCell><VisibilityIcon onClick={ () => handleViewDetail( employee.id ) } /> </TableCell>
              </TableRow>
            ) )
            : <TableRow>
              <TableCell> No content </TableCell>
            </TableRow> }
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const mapStateToProps = ( state: IAppState ): IOwnReduxStateProps => ( {
  list: getEmployeesFormattedList( state ),
} );


const mapDispatchToProps = ( dispatch: Dispatch ): IOwnReduxDispatchProps => ( {
  handleViewDetail:    ( id: number ) => {
    dispatch( setDetailActiveId( id ) );
    dispatch( setDetailPopupMode( EDetailPopupModes.view ) );
  },
} );

export default connect( mapStateToProps, mapDispatchToProps )( EmployeeList );
