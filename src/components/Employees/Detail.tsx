import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import {
  Button, createStyles, Grid, InputAdornment, MenuItem, Modal, TextField, Theme, Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { EDetailPopupModes, IAction, IAppState } from '~definitions';
import currencies from '~constants/currencies';

import { getActiveEmployee, getDetailModeSel } from '~redux/detail/selectors';
import { addEmployee } from '~redux/employees/actions';
import { IEmployee, IEmployeeAction } from '~redux/employees/definitions';
import { resetDetail } from '~redux/detail/actions';

import { formatEmployee } from '~utils/employee.util';
import { useInput } from '~utils/react.util';

interface IOwnReduxStateProps {
  detailData: IEmployee | undefined;
  mode: EDetailPopupModes
}

interface IOwnReduxDispatchProps {
  hidePopup: () => IAction;
  handleSaveEmployee: ( employee: IEmployee ) => IEmployeeAction;
}

type TAllProps = IOwnReduxStateProps & IOwnReduxDispatchProps;

const useStyles = makeStyles( ( theme: Theme ) => createStyles( {
    paper:   {
      position:        'absolute',
      width:           400,
      backgroundColor: theme.palette.background.paper,
      boxShadow:       theme.shadows[5],
      padding:         theme.spacing( 2, 4, 3 ),
      left:            '50%',
      top:             '50%',
      transform:       'translate(-50%, -50%)',
    },
    section: {
      marginBottom: theme.spacing( 2 ),
    },
  } ),
);


/**
 * Component that serves to display & add information of employees
 * @param props.detailData - Data that will be populated if its in view mode
 * @param props.mode - Serves as a layout ordering in the component
 * @param props.hidePopup - Action to hide the popup
 * @param props.handleSaveEmployee - Action to handle the new employee info
 */
const Detail = ( props: TAllProps ) => {
  const { detailData, mode, hidePopup, handleSaveEmployee } = props;

  const { value: firstName, bind: bindFirstName } = useInput( detailData?.first_name || '' );
  const { value: lastName, bind: bindLastName } = useInput( detailData?.last_name || '' );
  const { value: salary, bind: bindSalary } = useInput( detailData?.annual_salary.amount || '' );
  const { value: currency, bind: bindCurrency } = useInput( detailData?.annual_salary.currency || 'USD' );
  const { value: startDate, bind: bindStartDate } = useInput( detailData?.start_date || '2016-07-10' );

  const isViewMode = mode === EDetailPopupModes.view;
  const classes = useStyles();

  const handleSave = () => {
    // Parse any JSON previously stored in employeesList
    let existingEntries = JSON.parse( localStorage.getItem( 'employeesList' ) );
    if ( existingEntries === null ) existingEntries = [];
    const employee: IEmployee = formatEmployee( existingEntries.length + 1, firstName, lastName, salary, currency, startDate );

    // Save allEntries back to local storage
    existingEntries.push( employee );
    localStorage.setItem( 'employeesList', JSON.stringify( existingEntries ) );

    handleSaveEmployee( employee );
    hidePopup();
  };

  const createPDF = () => {
    let style = '<style>';
    style = style + 'table {width: 100%;font: 17px Calibri;}';
    style = style + 'table, th, td {border: solid 1px #DDD; border-collapse: collapse;';
    style = style + 'padding: 2px 3px;text-align: center;}';
    style = style + '</style>';

    const win = window.open( '', '', 'height=450,width=700' );

    win.document.write( '<html><head>' );
    win.document.write( '<title>Employee Detail</title>' );
    win.document.write( style );
    win.document.write( '</head>' );
    win.document.write( '<body>' );
    win.document.write( '<table>' );
    win.document.write( '<tr> <th>First Name</th> <th>Last Name</th> </tr>' );
    win.document.write( `<tr> <td>${ firstName }</td> <td>${ lastName }</td> </tr>` );
    win.document.write( '<tr> <th>Salary</th> <th>Start Date</th> </tr>' );
    win.document.write( `<tr> <td>${ salary } ${ currencies[currency] }</td> <td>${ startDate }</td> </tr>` );
    win.document.write( '</table>' );
    win.document.write( '</body></html>' );

    win.document.close();

    win.print();
  }

  const handleSubmit = ( e: React.FormEvent<HTMLFormElement> ) => {
    e.preventDefault();

    if ( isViewMode ) {
      createPDF();
    } else {
      handleSave();
    }
  };

  return (
    <Modal
      open={ true }
      onClose={ hidePopup }
    >
      <form className={ classes.paper } onSubmit={ ( e ) => handleSubmit( e ) }>
        <div className={ classes.section }>
          <Grid item xs={ 12 }>
            <Typography variant='h6'>
              { isViewMode ? 'Detail' : 'Add new Employee' }
            </Typography>
          </Grid>
        </div>

        <div className={ classes.section }>
          <Grid container spacing={ 3 }>

            <Grid item xs={ 12 } sm={ 6 }>
              <TextField fullWidth variant='outlined' label='First Name' { ...bindFirstName } required
                         disabled={ isViewMode } />
            </Grid>
            <Grid item xs={ 12 } sm={ 6 }>
              <TextField fullWidth variant='outlined' label='Last Name' { ...bindLastName } required
                         disabled={ isViewMode } />
            </Grid>

            <Grid item xs={ 12 } sm={ 6 }>
              <TextField fullWidth variant='outlined' label='Salary Amount' { ...bindSalary } required
                         disabled={ isViewMode } type='number'
                         InputProps={ {
                           startAdornment: <InputAdornment position='start'>
                                             { currencies[currency] || '$' }</InputAdornment>,
                         } }
              />
            </Grid>
            <Grid item xs={ 12 } sm={ 6 }>
              <TextField fullWidth variant='outlined' label='Salary Currency' select { ...bindCurrency } required
                         disabled={ isViewMode }>
                { Object.keys( currencies ).map( ( key ) => (
                  <MenuItem key={ key } value={ key }>
                    { currencies[key] }
                  </MenuItem>
                ) ) }
              </TextField>
            </Grid>

            <Grid item xs={ 12 }>
              <TextField fullWidth variant='outlined' label='Start Date' type='date' { ...bindStartDate }
                         disabled={ isViewMode } />
            </Grid>
          </Grid>
        </div>

        <Grid item xs={ 12 } container justify='flex-end'>
          <Button variant='contained' color='primary' type='submit'>
            { isViewMode ? 'Export PDF' : 'Add' }
          </Button>
        </Grid>
      </form>
    </Modal>
  );
};

const mapStateToProps = ( state: IAppState ): IOwnReduxStateProps => ( {
  detailData: getActiveEmployee( state ),
  mode:       getDetailModeSel( state ),
} );

const mapDispatchToProps = ( dispatch: Dispatch ): IOwnReduxDispatchProps => bindActionCreators( {
  hidePopup:          () => resetDetail(),
  handleSaveEmployee: ( employee: IEmployee ) => addEmployee( employee ),
}, dispatch );

export default connect( mapStateToProps, mapDispatchToProps )( Detail );
