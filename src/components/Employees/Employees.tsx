import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Grid, InputAdornment, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';

import { EDetailPopupModes, IAppState } from '~definitions';
import { setDetailPopupMode, setDetailVisibility } from '~redux/detail/actions';
import { getDetailVisibilitySel } from '~redux/detail/selectors';

import EmployeeList from '~components/Employees/List';
import Detail from '~components/Employees/Detail';

import { useInput } from '~utils/react.util';

const useStyles = makeStyles( ( theme ) => ( {
  layout:      {
    width:                                                  'auto',
    marginLeft:                                             theme.spacing( 2 ),
    marginRight:                                            theme.spacing( 2 ),
    [theme.breakpoints.up( 800 + theme.spacing( 2 ) * 2 )]: {
      width:       800,
      marginLeft:  'auto',
      marginRight: 'auto',
    },
  },
  paper:       {
    marginTop:                      theme.spacing( 3 ),
    marginBottom:                   theme.spacing( 3 ),
    padding:                        theme.spacing( 2 ),
    [theme.breakpoints.up( 'md' )]: {
      marginTop:    theme.spacing( 6 ),
      marginBottom: theme.spacing( 6 ),
      padding:      theme.spacing( 3 ),
    },
  },
  newButton:   {
    [theme.breakpoints.up( 'sm' )]:   {
      float: 'right',
    },
    [theme.breakpoints.down( 'xs' )]: {
      width:        '100%',
      marginBottom: theme.spacing( 6 ),
    },
  },
  searchInput: {
    [theme.breakpoints.up( 'sm' )]: {
      marginBottom: theme.spacing( 2 ),
    },
  },
} ) );

interface IOwnReduxStateProps {
  isDetailPopupVisible: boolean;
}

interface IOwnReduxDispatchProps {
  handleAddNewEmployee: () => void;
}

type TAllProps = IOwnReduxStateProps & IOwnReduxDispatchProps;

/**
 * Main Component that serves as Layout for its child components
 * @param isDetailPopupVisible - Will determine if popup shows or not
 * @param handleAddNewEmployee - Action to handle the showing of new employee popup
 */
const Employees = ( { isDetailPopupVisible, handleAddNewEmployee }: TAllProps ): JSX.Element => {
  const { value: nameFilter, bind: bindNameFilter } = useInput( '' );

  const classes = useStyles();

  return (
    <React.Fragment>
      <main className={ classes.layout }>
        <Paper className={ classes.paper }>
          <Grid container spacing={ 3 }>
            <Grid item xs={ 12 } sm={ 6 }>
              <TextField fullWidth label='Search by Name' className={ classes.searchInput } { ...bindNameFilter }
                         InputProps={ { startAdornment: ( <InputAdornment
                             position='start'><SearchIcon /></InputAdornment> ),
                         } }
              />
            </Grid>
            <Grid item xs={ 12 } sm={ 6 }>
              <Button variant='contained' color='primary' className={ classes.newButton } startIcon={ <AddIcon /> }
                      onClick={ handleAddNewEmployee }>
                Add new
              </Button>
            </Grid>
          </Grid>

          <EmployeeList nameFilter={ nameFilter } />
        </Paper>
      </main>

      { isDetailPopupVisible && <Detail /> }
    </React.Fragment>
  );
};

const mapStateToProps = ( state: IAppState ): IOwnReduxStateProps => ( {
  isDetailPopupVisible: getDetailVisibilitySel( state ),
} );

const mapDispatchToProps = ( dispatch: Dispatch ): IOwnReduxDispatchProps => ( {
  handleAddNewEmployee: () => {
    dispatch( setDetailVisibility( true ) );
    dispatch( setDetailPopupMode( EDetailPopupModes.new ) );
  },
} );

export default connect( mapStateToProps, mapDispatchToProps )( Employees );
