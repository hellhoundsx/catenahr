import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { AppBar, CssBaseline, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import mockData from '~constants/mockData';
import { componentDidMount } from '~utils/react.util';

import { IEmployee, IEmployeeListAction } from '~redux/employees/definitions';
import { setEmployeeList } from '~redux/employees/actions';

import Employees from '~components/Employees/Employees';

const useStyles = makeStyles( () => ( {
  appBar: {
    position: 'relative',
  },
} ) );


interface IOwnReduxDispatchProps {
  saveLocalData: ( list: IEmployee[] ) => IEmployeeListAction
}

type TAllProps = IOwnReduxDispatchProps;

const App = ( { saveLocalData }: TAllProps ) => {
  const classes = useStyles();

  componentDidMount( () => {
    // Parse any JSON previously stored in employeesList
    const existingEntries = JSON.parse( localStorage.getItem( 'employeesList' ) );

    if ( existingEntries?.length > 0 ) {
      // Populate the global state with existing configs
      saveLocalData( existingEntries );
    } else {
      // Populate the global state with default configs
      saveLocalData( mockData );
      // Populate the local storage as well
      localStorage.setItem( 'employeesList', JSON.stringify( mockData ) );
    }
  } );

  return (
    <div className='app'>
      <CssBaseline />
      <AppBar position='absolute' color='default' className={classes.appBar}>
        <Toolbar>
          <Typography variant='h6' color='inherit' noWrap>
            Catena HR - Employees
          </Typography>
        </Toolbar>
      </AppBar>

      <Employees />
    </div>
  );
}

const mapDispatchToProps = ( dispatch: Dispatch ): IOwnReduxDispatchProps => bindActionCreators( {
  saveLocalData: ( list: IEmployee[] ) => setEmployeeList( list ),
}, dispatch );

export default connect( null, mapDispatchToProps )( App );
