# Catena HR
> Project developed for the tech assigment of Catena Media

Project used with [react-webpack-typescript-starter](https://github.com/vikpe/react-webpack-typescript-starter) as a base.

* **[React](https://facebook.github.io/react/)** (17.x)
* **[Webpack](https://webpack.js.org/)** (4.x)
* **[Typescript](https://www.typescriptlang.org/)** (4.x)
* **[Material UI](https://material-ui.com/)** (4.x)


##Notes
This software was developed using only the techologies asked in the initial README.md

No other libraries were used due to that (classnames, reselect, react-router, etc...).

Development cost: 10h (including setup)

## Installation
1. Clone/download repo
2. `yarn install` (or `npm install` for npm)

## Usage
`yarn run start-dev`

* Build app continuously (HMR enabled)
* App served @ `http://localhost:8080`
